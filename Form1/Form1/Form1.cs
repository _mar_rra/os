﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Form1
{
    public partial class Form1 : Form
    {
        static private Socket Client;
        private IPAddress ip = null;
        private int port = 0;
        private Thread th;//для потока

        public Form1()
        {
            InitializeComponent();
            richTextBox2.ReadOnly = true;
            richTextBox1.Enabled = false;
           // richTextBox3.Enabled = false;
            //buttonUpPriv.Enabled = false;
            buttonUP.Enabled = false;
            try
            {
                var sr = new StreamReader(@"Client_info/data_info.txt");
                string buffer = sr.ReadToEnd();
                sr.Close();
                string[] connect_info = buffer.Split(':'); //Разделяет ip и порт в файле
                ip = IPAddress.Parse(connect_info[0]); //Переводит строковое в значание integer

                port = int.Parse(connect_info[1]); //Переводит строковое в значание integer

                label4.ForeColor = Color.Black;
                label4.Text = "Настройки: \n IP сервера: " + connect_info[0] + "\n Порт сервера: " + connect_info[1];

            }

            catch (Exception ex)
            {

                label4.ForeColor = Color.Red;
                label4.Text = "Настройки не найдены!";
                Form2 form = new Form2();
                form.Show();

            }
           // button1.Enabled = false;
          
        }

        void SendMessage(string message) // Отправляет сообщение на сервер
        {

            if (message != " " && message != "") //Проверка на пустое сообщение
            {

                byte[] buffer = new byte[1024];
                buffer = Encoding.UTF8.GetBytes(message); //Отправка сообщение на сервер в виде байт кода

                Client.Send(buffer);

            }

        }

        void RecvMessage()
        {

            byte[] buffer = new byte[1024];
            for (int i = 0; i < buffer.Length; i++) //чистка буфера
            {
                buffer[i] = 0;
            }
            for (; ; )
            {
                try
                {
                    Client.Receive(buffer); //Принимает сообщение
                    string message = Encoding.UTF8.GetString(buffer); //Переводм байт код в сообщение
                    string[] mes = message.Split(':');

                    for (int i = 0; i < buffer.Length; i++) // чистка буфера
                    {
                        buffer[i] = 0;
                    }

                    this.Invoke((MethodInvoker)delegate()
                    {
                        if (mes.Count() != 1)
                        {
                            if (mes[1] == Name.Text || mes[1] == "" || mes[1] == Wname.Text)
                                richTextBox2.AppendText(mes[0] + ": " + mes[2] + "\n"); //Выводим сообщение
                            else
                                richTextBox2.AppendText(mes[0]);
                        }

                    });

                }

                catch (SocketException ex) 
                {

                    MessageBox.Show("Клиент отключен от сервера. Попробуйти войти еще раз", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    InitializeComponent();
                    //richTextBox2.ReadOnly = false;
                    //richTextBox1.Enabled = false;
                    //buttonUP.Enabled = false;
                    richTextBox2.Clear();
                    Name.Clear();
                }

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Message_TextChanged(object sender, EventArgs e)
        {

        }
      

        private void buttonUP_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "" && richTextBox1.Text != " ")
            {
                SendMessage(Name.Text + ":" + Wname.Text + ":" + richTextBox1.Text + "\n");
                richTextBox1.Clear();
            }

            else
            {

                MessageBox.Show("Вы не ввели сообщение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            
        }

        private void Chat_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (Name.Text != " " && Name.Text != "")
            {
                buttonUP.Enabled = true;
                //buttonUpPriv.Enabled = true;
                richTextBox1.Enabled = true;
                //richTextBox3.Enabled = true;
                buttonConnect.Enabled = false;
                Name.Enabled = false;
                Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //Сокет для подключения

                if (ip != null)
                {
                    Client.Connect(ip, port); //Подключение по заданным настройкам
                    SendMessage(Name.Text + ":: connected to chat" + "\n");
                    th = new Thread(delegate() { RecvMessage(); }); //Поток принимающий сообщения от сервера
                    
                    th.Start();
                }
            }
            else
            {
                MessageBox.Show("Введите никнейм!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox2.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 Form3 = new Form3();
            Form3.ShowDialog();
        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Name.Text != " " && Name.Text != "")

                SendMessage(Name.Text + " disconnected from chat\n");

            if (th != null)
            {

                th.Abort();

            }

            if (Client != null)
            {

                Client.Close();

            }
            Application.Exit();

        }
      
        private void label1_Click_1(object sender, EventArgs e)
        {

        }

   

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            form.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Name.Text != " " && Name.Text != "")

                SendMessage(Name.Text + " disconnected from chat\n");

            if (th != null)
            {

                th.Abort();

            }

            if (Client != null)
            {

                Client.Close();

            }
        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "" && richTextBox1.Text != " ")
            {
                SendMessage(Name.Text + ":" + Wname.Text + ":" + richTextBox1.Text + "\n");
                richTextBox1.Clear();
            }

            else
            {

                MessageBox.Show("Вы не ввели сообщение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        private void Wname_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
